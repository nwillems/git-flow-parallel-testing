#/bin/bash -xe

git config --add commit.gpgsign false

version=`cat version.txt | grep '^compB' | sed 's/compB: \(.*\)/\1/'`
HOTFIX_NUMBER=$(( $version +1))

git flow hotfix start hf-$HOTFIX_NUMBER

./bump_version.sh "compB" $HOTFIX_NUMBER
git commit -m "Version bump $HOTFIX_NUMBER"

git flow hotfix publish hf-$HOTFIX_NUMBER
echo "Running tests - usually takes 10secs"
sleep 10 # Run tests
echo "Test success - hotfixing"

tries=0
merged=1
while [[ $tries -lt 3 && $merged -eq 1 ]]; do
    echo "Updating master and develop"
    git checkout master && git pull
    git checkout develop && git pull

    echo "Hotfix - merging to master"
    export GIT_MERGE_AUTOEDIT=no
    git flow hotfix finish -k -p -F -m"Release v$HOTFIX_NUMBER" hf-$HOTFIX_NUMBER
    merged=$?
    tries=$(($tries + 1))

    echo "Result is merged=$merged and tries=$tries"
done

# Now it should be done
git flow hotfix delete -f $HOTFIX_NUMBER
