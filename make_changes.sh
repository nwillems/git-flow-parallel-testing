#/bin/bash -xe

NUM_CHANGES=${1:-1}

echo "Making $NUM_CHANGES changes to code.txt"

COUNT=0
while [ $COUNT -lt $NUM_CHANGES ]; do
    LC_CTYPE=C
    # Just some random strings - 20 chars long
    CHANGE=$(cat /dev/random | tr -dc "[:alpha:]" | head -c 20 && echo)

    echo $CHANGE >> code.txt
    
    mv code.txt code_old.txt
    tail -n30 code_old.txt > code.txt
    rm code_old.txt

    # Counting number of changes...
    let COUNT=COUNT+1
done

git add code.txt
git commit -m"Adding some code"
