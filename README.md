Welcome to git-flow parallel testing

This project is intended to test the amount of parallelism you can achieve when doing git-flow.
It will contain 3 types of scripts - 1 for doing development and committing to the dev branch, 1 for doing hotfixes and 1 for releases.