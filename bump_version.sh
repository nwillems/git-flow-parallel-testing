#/bin/bash

COMPONENT=$1
VERSION=$2

sed -i '' "s/\(${COMPONENT}:\) \(.*\)/\1 ${VERSION}/" version.txt

# We changed it - so we add it
git add version.txt
