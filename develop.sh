#/bin/bash -xe

rand_name=$(( $RANDOM % 100 +100 ))
git flow feature start $rand_name

./make_changes.sh
sleep 2

git flow feature finish -F -r --push $rand_name
