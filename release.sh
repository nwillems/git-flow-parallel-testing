#/bin/bash -xe

git config --add commit.gpgsign false

version=`cat version.txt | grep '^compA' | sed 's/compA: \(.*\)/\1/'`
RELEASE_NUMBER=$(( $version +10))

git flow release start $RELEASE_NUMBER

./bump_version.sh "compA" $RELEASE_NUMBER
git commit -m "Version bump $RELEASE_NUMBER"

git flow release publish $RELEASE_NUMBER
echo "Running tests - usually takes 30secs"
sleep 10 # Run tests
echo "Test success - releasing"

tries=0
merged=1
while [[ $tries -lt 3 && $merged -eq 1 ]]; do
    echo "Updating master and develop"
    git checkout master && git pull
    git checkout develop && git pull

    echo "Release - merging to master"
    export GIT_MERGE_AUTOEDIT=no
    PUSH_TO="--pushdevelop --pushproduction --pushtag"
    git flow release finish $PUSH_TO -k -F -m"Release v$RELEASE_NUMBER" $RELEASE_NUMBER
    merged=$?
    tries=$(($tries + 1))

    echo "Result is merged=$merged and tries=$tries"
done

# Now it should be done
git flow release delete -f $RELEASE_NUMBER
